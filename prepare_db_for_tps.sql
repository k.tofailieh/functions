-- Public 

CREATE OR REPLACE FUNCTION public.prepare_db_for_tps()
 RETURNS TABLE(schema_name character varying, table_name character varying, constraint_name character varying, column_name character varying, execute_ddl_clause character varying)
 LANGUAGE plpgsql
AS $function$
DECLARE
	constraint_instance RECORD;
	index_instance RECORD;
	execute_DDL_clause VARCHAR;
	temporal_db_status VARCHAR;
	db_name					VARCHAR := current_database();

	record_instance        RECORD;
    create_index_statement VARCHAR;
    table_name             VARCHAR;
    column_name            VARCHAR;
    schema_name            VARCHAR;
   
BEGIN
/* Remove FK constraints as far as destination is no more unique in time */
/* Remove unicity constraints as far as data are no more unique in time */
/* Replace PK and unique constraints by a simple btree index */

  temporal_db_status = case when db_name ~ '^.*-tps(-[a-z]+){0,1}$' then 'DST' else 'SRC' end;
  -- ASSERT databasename is "*-tps" or "*-tps-*". Support xmat-dev and xmat-tps-kyoto

  IF (temporal_db_status NOT IN ('DST')) THEN
	RAISE EXCEPTION 'prepare_db_for_tps FAILS : "ecomundo.temporal_db_status" <> DST : "%"', temporal_db_status;
  END IF;

FOR constraint_instance IN
            SELECT columns.table_schema, columns.table_name, ist_pk.constraint_schema, ist_pk.constraint_name, ist_pk.constraint_type, ist_pk_col.column_name
          FROM information_schema.columns /* Columns tpsuid of tables that are temporal (or ft tables)*/
          INNER JOIN INFORMATION_SCHEMA.TABLES /* base tables  (that are temporal (or ft tables) ) */
            ON (TABLES.table_schema, TABLES.table_name) = (columns.table_schema, columns.table_name)
            AND TABLES.table_type = 'BASE TABLE'
          INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS ist_pk /* Constraints (except temporal) */
            ON ist_pk.table_catalog = TABLES.table_catalog
            AND ist_pk.table_schema = TABLES.table_schema
            AND ist_pk.table_name = TABLES.table_name
          	AND ist_pk.constraint_type in ('PRIMARY KEY','FOREIGN KEY','UNIQUE') /* Very few unique because we use "unique index" instead (but is is not a good practice, unique constraint is better) */
          	AND ist_pk.constraint_name not like 'uc_%_tpsuid'
          	AND ist_pk.constraint_name not like 'uc_%_id_tpsfrom'
          INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE ist_pk_col /* The contraint column */
            ON ist_pk_col.table_catalog = ist_pk.table_catalog
            AND ist_pk_col.table_schema = ist_pk.table_schema
            AND ist_pk_col.table_name = ist_pk.table_name
            AND ist_pk_col.constraint_name = ist_pk.constraint_name
          WHERE columns.column_name in ('tpsuid', 'tsv') /* For tables with tps decoration (or ft tables) */
          AND columns.table_schema <> 'tps'
          ORDER BY ist_pk.constraint_type
    LOOP
		schema_name := constraint_instance.table_schema;
        table_name := constraint_instance.table_name;
        constraint_name := constraint_instance.constraint_name;
        column_name := constraint_instance.column_name;
		/* Remove FK constraint as far as destination is no more unique in time */
		/* Remove unicity constraint as far as data are no more unique in time */
        execute_DDL_clause :=  $$ALTER TABLE $$ || schema_name || $$."$$ || table_name || $$" DROP CONSTRAINT IF EXISTS "$$ || constraint_name || $$" RESTRICT$$;
        RAISE NOTICE '%',execute_DDL_clause;
        EXECUTE execute_DDL_clause;

		RETURN NEXT;
        
        IF (constraint_instance.constraint_type in ('PRIMARY KEY', 'UNIQUE')) THEN
        	-- Replace PK and unique constraint by an simple btree index
            execute_DDL_clause :=  $$CREATE INDEX IF NOT EXISTS itps_$$ || constraint_name || $$ ON $$ || schema_name || $$."$$ || table_name || $$" USING btree ("$$ || column_name || $$")$$;
            RAISE NOTICE '%',execute_DDL_clause;
            EXECUTE execute_DDL_clause;
       		RETURN NEXT;
        END IF;
    END LOOP;

	
/* Replace unique indexes by simple btree indexes */
/* It is important that the replacement of PK and unique constraints has been done before. This is for the rest like (entr_src_id, entr_dst_id, entr_tp) */
FOR index_instance IN
          		SELECT columns.table_schema, columns.table_name, columns.column_name, pg_indexes.schemaname, pg_indexes.indexname, pg_indexes.indexdef, replace(pg_indexes.indexdef,' UNIQUE','') AS newindex
          FROM information_schema.columns /* Columns tpsuid of tables that are temporal */
          INNER JOIN pg_indexes
            ON (schemaname, tablename) = (columns.table_schema, columns.table_name)
          WHERE columns.column_name = 'tpsuid'
          AND columns.table_schema <> 'tps'
          AND pg_indexes.indexdef ilike 'CREATE%UNIQUE%INDEX%'
          AND pg_indexes.indexdef not ilike '%(tpsuid)%'
          AND pg_indexes.indexname not like 'uc_%_tpsuid'
          AND pg_indexes.indexname not like 'uc_%_id_tpsfrom'
    LOOP
		schema_name := index_instance.schemaname;
        table_name := index_instance.table_name;
        constraint_name := index_instance.indexname;
        column_name := index_instance.column_name;
        execute_DDL_clause :=  $$DROP INDEX IF EXISTS $$ || schema_name || $$."$$ || constraint_name || $$"$$;
        RAISE NOTICE '%',execute_DDL_clause;
        EXECUTE execute_DDL_clause;
		RETURN NEXT;

        execute_DDL_clause :=  index_instance.newindex;
        RAISE NOTICE '%',execute_DDL_clause;
        EXECUTE execute_DDL_clause;
		RETURN NEXT;
    END LOOP;
  
   
/*Add Index on tpstill column in all tables contains it.*/
   FOR record_instance IN
    SELECT columns.table_schema,
           columns.table_name,
           columns.column_name,
           pg_indexes.indexname
           
-- get all tables that contains tpstill column without index on it.
    FROM information_schema.columns
             LEFT JOIN pg_indexes
                       ON (pg_indexes.schemaname, pg_indexes.tablename) =
                          (columns.table_schema, columns.table_name)
                           AND pg_indexes.indexname ILIKE ('%_tpstill')
    WHERE columns.column_name ILIKE ('%tpstill')
      AND pg_indexes.indexname ISNULL
      
    LOOP
        table_name  := record_instance.table_name;
        column_name := record_instance.column_name;
        schema_name := record_instance.table_schema;
       
        create_index_statement := $$CREATE INDEX $$ || $$i_$$ || table_name || $$_$$ || column_name || $$ ON $$ ||
                                  schema_name || $$."$$ ||
                                  table_name || $$" USING btree ($$ || column_name || $$);$$;
        BEGIN
	    EXECUTE create_index_statement;
        RAISE NOTICE '%, Done', create_index_statement;
       
        EXCEPTION WHEN OTHERS THEN
            RAISE EXCEPTION 'Some error happen when execute: %', create_index_statement;
        END;
    END LOOP;

END;
$function$
;

COMMENT ON FUNCTION public.prepare_data_for_tps() IS 
'Prepare the DB to be able to hold temporal data.
PK are replaced by simple indexes (1).
FK are removed.
UNIQUE constraints are replaced by simple indexes (1)
UNIQUE indexes are replaced by simple indexes (1)(2)
BTREE indexes are added into tpstill column in all tables that contains it 
This function can be played again and again. It is not destructive.

(1) Works only if UNIQUE constraint and PK are on a single column. Composite index are not supported.
(2) To set a unicity constraint, you can declare a constraint or a unique index. Actually, unique indexes are the most used way but it is a bad practice and table constraints should be prefered.

'





-------------------------------------------------------------------------------------------------------------------
-- DPTps

CREATE OR REPLACE FUNCTION tps.prepare_db_for_tps()
 RETURNS TABLE(schema_name character varying, table_name character varying, constraint_name character varying, column_name character varying, execute_ddl_clause character varying)
 LANGUAGE plpgsql
AS $function$
DECLARE
	constraint_instance RECORD;
	index_instance RECORD;
	execute_DDL_clause VARCHAR;
	temporal_db_status VARCHAR;
	db_name					VARCHAR := current_database();

	record_instance        RECORD;
    create_index_statement VARCHAR;
    table_name             VARCHAR;
    column_name            VARCHAR;
    schema_name            VARCHAR;
   
BEGIN
/* Remove FK constraints as far as destination is no more unique in time */
/* Remove unicity constraints as far as data are no more unique in time */
/* Replace PK and unique constraints by a simple btree index */

  temporal_db_status = case when db_name ~ '^.*-tps(-[a-z]+){0,1}$' then 'DST' else 'SRC' end;
  -- ASSERT databasename is "*-tps" or "*-tps-*". Support xmat-dev and xmat-tps-kyoto

  IF (temporal_db_status NOT IN ('DST')) THEN
	RAISE EXCEPTION 'prepare_db_for_tps FAILS : "ecomundo.temporal_db_status" <> DST : "%"', temporal_db_status;
  END IF;

FOR constraint_instance IN
            SELECT columns.table_schema, columns.table_name, ist_pk.constraint_schema, ist_pk.constraint_name, ist_pk.constraint_type, ist_pk_col.column_name
          FROM information_schema.columns /* Columns tpsuid of tables that are temporal (or ft tables)*/
          INNER JOIN INFORMATION_SCHEMA.TABLES /* base tables  (that are temporal (or ft tables) ) */
            ON (TABLES.table_schema, TABLES.table_name) = (columns.table_schema, columns.table_name)
            AND TABLES.table_type = 'BASE TABLE'
          INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS ist_pk /* Constraints (except temporal) */
            ON ist_pk.table_catalog = TABLES.table_catalog
            AND ist_pk.table_schema = TABLES.table_schema
            AND ist_pk.table_name = TABLES.table_name
          	AND ist_pk.constraint_type in ('PRIMARY KEY','FOREIGN KEY','UNIQUE') /* Very few unique because we use "unique index" instead (but is is not a good practice, unique constraint is better) */
          	AND ist_pk.constraint_name not like 'uc_%_tpsuid'
          	AND ist_pk.constraint_name not like 'uc_%_id_tpsfrom'
          INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE ist_pk_col /* The contraint column */
            ON ist_pk_col.table_catalog = ist_pk.table_catalog
            AND ist_pk_col.table_schema = ist_pk.table_schema
            AND ist_pk_col.table_name = ist_pk.table_name
            AND ist_pk_col.constraint_name = ist_pk.constraint_name
          WHERE columns.column_name in ('tpsuid', 'tsv') /* For tables with tps decoration (or ft tables) */
          AND columns.table_schema <> 'tps'
          ORDER BY ist_pk.constraint_type
    LOOP
		schema_name := constraint_instance.table_schema;
        table_name := constraint_instance.table_name;
        constraint_name := constraint_instance.constraint_name;
        column_name := constraint_instance.column_name;
		/* Remove FK constraint as far as destination is no more unique in time */
		/* Remove unicity constraint as far as data are no more unique in time */
        execute_DDL_clause :=  $$ALTER TABLE $$ || schema_name || $$."$$ || table_name || $$" DROP CONSTRAINT IF EXISTS "$$ || constraint_name || $$" RESTRICT$$;
        RAISE NOTICE '%',execute_DDL_clause;
        EXECUTE execute_DDL_clause;

		RETURN NEXT;
        
        IF (constraint_instance.constraint_type in ('PRIMARY KEY', 'UNIQUE')) THEN
        	-- Replace PK and unique constraint by an simple btree index
            execute_DDL_clause :=  $$CREATE INDEX IF NOT EXISTS itps_$$ || constraint_name || $$ ON $$ || schema_name || $$."$$ || table_name || $$" USING btree ("$$ || column_name || $$")$$;
            RAISE NOTICE '%',execute_DDL_clause;
            EXECUTE execute_DDL_clause;
       		RETURN NEXT;
        END IF;
    END LOOP;

	
/* Replace unique indexes by simple btree indexes */
/* It is important that the replacement of PK and unique constraints has been done before. This is for the rest like (entr_src_id, entr_dst_id, entr_tp) */
FOR index_instance IN
          		SELECT columns.table_schema, columns.table_name, columns.column_name, pg_indexes.schemaname, pg_indexes.indexname, pg_indexes.indexdef, replace(pg_indexes.indexdef,' UNIQUE','') AS newindex
          FROM information_schema.columns /* Columns tpsuid of tables that are temporal */
          INNER JOIN pg_indexes
            ON (schemaname, tablename) = (columns.table_schema, columns.table_name)
          WHERE columns.column_name = 'tpsuid'
          AND columns.table_schema <> 'tps'
          AND pg_indexes.indexdef ilike 'CREATE%UNIQUE%INDEX%'
          AND pg_indexes.indexdef not ilike '%(tpsuid)%'
          AND pg_indexes.indexname not like 'uc_%_tpsuid'
          AND pg_indexes.indexname not like 'uc_%_id_tpsfrom'
    LOOP
		schema_name := index_instance.schemaname;
        table_name := index_instance.table_name;
        constraint_name := index_instance.indexname;
        column_name := index_instance.column_name;
        execute_DDL_clause :=  $$DROP INDEX IF EXISTS $$ || schema_name || $$."$$ || constraint_name || $$"$$;
        RAISE NOTICE '%',execute_DDL_clause;
        EXECUTE execute_DDL_clause;
		RETURN NEXT;

        execute_DDL_clause :=  index_instance.newindex;
        RAISE NOTICE '%',execute_DDL_clause;
        EXECUTE execute_DDL_clause;
		RETURN NEXT;
    END LOOP;
  
   
/*Add Index on tpstill column in all tables contains it.*/
   FOR record_instance IN
    SELECT columns.table_schema,
           columns.table_name,
           columns.column_name,
           pg_indexes.indexname
           
-- get all tables that contains tpstill column without index on it.
    FROM information_schema.columns
             LEFT JOIN pg_indexes
                       ON (pg_indexes.schemaname, pg_indexes.tablename) =
                          (columns.table_schema, columns.table_name)
                           AND pg_indexes.indexname ILIKE ('%_tpstill')
    WHERE columns.column_name ILIKE ('%tpstill')
      AND pg_indexes.indexname ISNULL

    LOOP
        table_name  := record_instance.table_name;
        column_name := record_instance.column_name;
        schema_name := record_instance.table_schema;
       
        create_index_statement := $$CREATE INDEX $$ || $$i_$$ || table_name || $$_$$ || column_name || $$ ON $$ ||
                                  schema_name || $$."$$ ||
                                  table_name || $$" USING btree ($$ || column_name || $$);$$;
        BEGIN
	    EXECUTE create_index_statement;
        RAISE NOTICE '%, Done.', create_index_statement;        
        EXCEPTION WHEN OTHERS THEN
            RAISE EXCEPTION 'Some error happen when execute: %', create_index_statement;
        END;
    END LOOP;

END;
$function$
;

COMMENT ON FUNCTION tps.prepare_data_for_tps() IS 
'Prepare the DB to be able to hold temporal data.
PK are replaced by simple indexes (1).
FK are removed.
UNIQUE constraints are replaced by simple indexes (1)
UNIQUE indexes are replaced by simple indexes (1)(2)
BTREE indexes are added into tpstill column in all tables that contains it 
This function can be played again and again. It is not destructive.

(1) Works only if UNIQUE constraint and PK are on a single column. Composite index are not supported.
(2) To set a unicity constraint, you can declare a constraint or a unique index. Actually, unique indexes are the most used way but it is a bad practice and table constraints should be prefered.

'
/* This Functions Reindex the links that related to some entites.
 * 
 * extract the enitiy_id that you want to change (duplications for example), and the entity_id you want to replacing by.
 * pass them as parameters with entity name.
 * it is important to keep the order of ids in the lists.
 * 
 *  Parameters: 
 *		 1- entity_name: table name in the database.(fiche for example).
 *		 2- old_ids: the entity_ids that we want to change then delete.
 *		 3- new_ids: the entity_ids that we want to put.
 * */

/*--------------------------------------function: reindex_entity_relations--------------------------------------*/
CREATE OR REPLACE FUNCTION public.reindex_entity_relations(entity_name character VARYING, old_ids bigint[], new_ids bigint[])
RETURNS void AS
$body$
DECLARE 
  update_statement varchar;
  delete_statement varchar;
  last_rowcount int;
  link_instance RECORD;
BEGIN
	/*
	 * Update Parameters:
	 * (1)table-schema, (2)table_name, (3)column_name(src/dst), (4)column_name(dst/src), (5)link_tp(%_tp column), (6)old_ids list, (7) new_ids list.
	 * */
    update_statement := $update_links$
		UPDATE %1$s.%2$s SET %3$s = new_.id
		FROM 
			(SELECT ROW_NUMBER() OVER () rn, entity.id FROM UNNEST(CAST(%6$L AS bigint[])) entity(id) ) old_
			INNER JOIN 
			(SELECT ROW_NUMBER() OVER () rn, entity.id FROM UNNEST(CAST(%7$L AS bigint[])) entity(id) ) new_ 
			USING(rn)
		WHERE old_.id = %3$s AND new_.id <> %3$s
		AND NOT EXISTS (SELECT 1 FROM %1$s.%2$s ctrl where (ctrl.%3$s, ctrl.%4$s, ctrl.%5$s) = (new_.id, %2$s.%4$s, %2$s.%5$s)) /* do not update if the new link is already exists. */
	 $update_links$;
	
	/*
	 * Delete Parameters:
	 * (1)table-schema, (2)table_name, (3)column_name(src/dst), (4)old_ids list.
	 * */
	delete_statement := $delete_remained_links$
	    DELETE FROM %1$s.%2$s
	    USING (SELECT  entity.old_id FROM UNNEST(CAST(%4$L AS bigint[])) entity(old_id) ) deleted_links   
	    WHERE  deleted_links.old_id = %3$s 
	 $delete_remained_links$;
	
/* Get links that related to the entity from the source. */
 RAISE NOTICE ' ----------------------------------------- Source ----------------------------------------- ';
 FOR link_instance IN 
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
    INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
    WHERE 
    	(
    	tabs.table_name LIKE ('%\_'|| entity_name ||'\_%')
    	OR tabs.table_name LIKE ANY (VALUES ('extr\_'|| entity_name),('docr\_'|| entity_name ),('der\_' || entity_name)) /* because extr_entity.extr_src_id for example is related with entity. */
    	)
	    AND  tabs.table_type = 'BASE TABLE' 
	    AND tabs.table_schema IN ('public', 'anx')
	    AND cols.column_name LIKE ('%_src_%')   
 LOOP
	 /* Update Links,  column_name is like %_src_% */	 
	 EXECUTE format(update_statement, link_instance.table_schema, link_instance.table_name,link_instance.column_name, REPLACE(link_instance.column_name, 'src', 'dst'), REPLACE(link_instance.column_name,'src_id','tp'), old_ids, new_ids);
	 GET DIAGNOSTICS last_rowcount = ROW_COUNT;
	 RAISE NOTICE '%.%: %, % rows updated.',link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount;

	 /* Delete remained links which didn't reindexed by the previous command. */	
	 EXECUTE format(delete_statement,link_instance.table_schema, link_instance.table_name, link_instance.column_name, old_ids, new_ids);
	GET DIAGNOSTICS last_rowcount = ROW_COUNT;	
	RAISE NOTICE '%.%: %, % rows deleted.', link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount; 
 END LOOP;
  
/* Get relations that links to the entity from the destination. */
 RAISE NOTICE ' -------------------------------------- Destination -------------------------------------- ';
 FOR link_instance IN
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
	FROM information_schema.COLUMNS cols 
	INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
	WHERE
	  ( 
	    (tabs.table_name LIKE ('%\_'|| entity_name)
	     AND NOT tabs.table_name LIKE ANY(VALUES ('extr\_%'),('docr\_%'),('der\_%'), ('ft\_%')) /* becaues extr_entity.extr_dst_id for example is related with ext not with entity.*/
	    )
    	OR tabs.table_name LIKE (entity_name ||'r\_%')/* the entity name is ext or doc for example.*/ 
      )
	AND tabs.table_type = 'BASE TABLE'
	AND tabs.table_schema IN ('public', 'anx')
	AND cols.column_name LIKE '%\_dst\_%'  
 LOOP 	 
	  /* Update links. column_name is like %_dst_% */    
	  EXECUTE format(update_statement,link_instance.table_schema, link_instance.table_name,link_instance.column_name,REPLACE(link_instance.column_name, 'dst', 'src'), REPLACE(link_instance.column_name, 'dst_id', 'tp'), old_ids, new_ids);
	  GET DIAGNOSTICS last_rowcount = ROW_COUNT;
	  RAISE NOTICE '%.%: %, % rows updated.',link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount;
	  
	 /* Delete remained links which didn't reindexed by the previous command. */	
	  EXECUTE format(delete_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, old_ids, new_ids);
	  GET DIAGNOSTICS last_rowcount = ROW_COUNT;	
	  RAISE NOTICE '%.%: %, % rows deleted.',link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount;	
 END LOOP; 
END
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.reindex_entity_relations(character VARYING,character VARYING) IS 
'This function is about reindexing all links that related to certain entity.
 Parameters: 
	 1- entity_name: table name in the database.(fiche for example).
	 2- old_ids: the entity_ids that we want to change then delete.
	 3- new_ids: the entity_ids that we want to put.';
	
------------------------------------------------------------------------------------------------------------------------

	
	
	
	
------------------------------------------------------------------------------------------------------------------------
 
/*
######  ######  #######               
#     # #     #    #    #####   ####  
#     # #     #    #    #    # #      
######  #     #    #    #    #  ####  
#     # #     #    #    #####       # 
#     # #     #    #    #      #    # 
######  ######     #    #       ####  
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$
/*--------------------------------------function: reindex_entity_relations--------------------------------------*/
CREATE OR REPLACE FUNCTION public.reindex_entity_relations(entity_name character VARYING, old_ids bigint[], new_ids bigint[])
RETURNS void AS
$body$
DECLARE 
  update_statement varchar;
  delete_statement varchar;
  last_rowcount int;
  link_instance RECORD;
BEGIN
	/*
	 * Update Parameters:
	 * (1)table-schema, (2)table_name, (3)column_name(src/dst), (4)column_name(dst/src), (5)link_tp(%_tp column), (6)old_ids list, (7) new_ids list.
	 * */
    update_statement := $update_links$
		UPDATE %1$s.%2$s SET %3$s = new_.id
		FROM 
			(SELECT ROW_NUMBER() OVER () rn, entity.id FROM UNNEST(CAST(%6$L AS bigint[])) entity(id) ) old_
			INNER JOIN 
			(SELECT ROW_NUMBER() OVER () rn, entity.id FROM UNNEST(CAST(%7$L AS bigint[])) entity(id) ) new_ 
			USING(rn)
		WHERE old_.id = %3$s AND new_.id <> %3$s
		AND NOT EXISTS (SELECT 1 FROM %1$s.%2$s ctrl where (ctrl.%3$s, ctrl.%4$s, ctrl.%5$s) = (new_.id, %2$s.%4$s, %2$s.%5$s)) /* do not update if the new link is already exists. */
	 $update_links$;
	
	/*
	 * Delete Parameters:
	 * (1)table-schema, (2)table_name, (3)column_name(src/dst), (4)old_ids list, (5) new_ids list.
	 * */
	delete_statement := $delete_remained_links$
	    DELETE FROM %1$s.%2$s
	    USING (
	          /* align old_ids and new_ids. */
		      (SELECT ROW_NUMBER() OVER () rn, entity.old_id FROM UNNEST(CAST(%4$L AS bigint[])) entity(old_id) ) old_
			   INNER JOIN 
			   (SELECT ROW_NUMBER() OVER () rn, entity.new_id FROM  UNNEST(CAST(%5$L AS bigint[])) entity(new_id) ) new_
			   USING(rn)
	          ) deleted_links
	    WHERE  deleted_links.old_id = %3$s AND deleted_links.new_id <> %3$s
	 $delete_remained_links$;
	
/* Get links that related to the entity from the source. */
 RAISE NOTICE ' ----------------------------------------- Source ----------------------------------------- ';
 FOR link_instance IN 
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
    INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
    WHERE 
    	(
    	tabs.table_name LIKE ('%\_'|| entity_name ||'\_%')
    	OR tabs.table_name LIKE ANY (VALUES ('extr\_'|| entity_name),('docr\_'|| entity_name ),('der\_' || entity_name)) /* because extr_entity.extr_src_id for example is related with entity. */
    	)
	    AND  tabs.table_type = 'BASE TABLE' 
	    AND tabs.table_schema IN ('public', 'anx')
	    AND cols.column_name LIKE ('%_src_%')   
 LOOP
	 /* Update Links,  column_name is like %_src_% */	 
	 EXECUTE format(update_statement, link_instance.table_schema, link_instance.table_name,link_instance.column_name, REPLACE(link_instance.column_name, 'src', 'dst'), REPLACE(link_instance.column_name,'src_id','tp'), old_ids, new_ids);
	 GET DIAGNOSTICS last_rowcount = ROW_COUNT;
	 RAISE NOTICE '%.%: %, % rows updated.',link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount;

	 /* Delete remained links which didn't reindexed by the previous command. */	
	 EXECUTE format(delete_statement,link_instance.table_schema, link_instance.table_name, link_instance.column_name, old_ids, new_ids);
	GET DIAGNOSTICS last_rowcount = ROW_COUNT;	
	RAISE NOTICE '%.%: %, % rows deleted.', link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount; 
 END LOOP;
  
/* Get relations that links to the entity from the destination. */
 RAISE NOTICE ' -------------------------------------- Destination -------------------------------------- ';
 FOR link_instance IN
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
	FROM information_schema.COLUMNS cols 
	INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
	WHERE
	  ( 
	    (tabs.table_name LIKE ('%\_'|| entity_name)
	     AND NOT tabs.table_name LIKE ANY(VALUES ('extr\_%'),('docr\_%'),('der\_%'), ('ft\_%')) /* becaues extr_entity.extr_dst_id for example is related with ext not with entity.*/
	    )
    	OR tabs.table_name LIKE (entity_name ||'r\_%')/* the entity name is ext or doc for example.*/ 
      )
	AND tabs.table_type = 'BASE TABLE'
	AND tabs.table_schema IN ('public', 'anx')
	AND cols.column_name LIKE '%\_dst\_%'  
 LOOP 	 
	  /* Update links. column_name is like %_dst_% */    
	  EXECUTE format(update_statement,link_instance.table_schema, link_instance.table_name,link_instance.column_name,REPLACE(link_instance.column_name, 'dst', 'src'), REPLACE(link_instance.column_name, 'dst_id', 'tp'), old_ids, new_ids);
	  GET DIAGNOSTICS last_rowcount = ROW_COUNT;
	  RAISE NOTICE '%.%: %, % rows updated.',link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount;
	  
	 /* Delete remained links which didn't reindexed by the previous command. */	
	  EXECUTE format(delete_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, old_ids, new_ids);
	  GET DIAGNOSTICS last_rowcount = ROW_COUNT;	
	  RAISE NOTICE '%.%: %, % rows deleted.',link_instance.table_schema, link_instance.table_name, link_instance.column_name, last_rowcount;	
 END LOOP; 
END
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.reindex_entity_relations(character VARYING,character VARYING) IS 
'This function is about reindexing all links that related to certain entity.
 Parameters: 
	 1- entity_name: table name in the database.(fiche for example).
	 2- old_ids: the entity_ids that we want to change then delete.
	 3- new_ids: the entity_ids that we want to put.';
	
$bdlink_exec_containt$);

SELECT dblink_disconnect('xmat-tps');

---------------------------------------------------------------------------------------------------------------------------



/* testing on Live DB.*/       
--SELECT reindex_entity_relations('substances', ARRAY[2744002], ARRAY[2744001]);



/*--------------------------------function: get_entity_links_count--------------------------------*/
CREATE OR REPLACE FUNCTION public.get_entity_links_count(entity_name character VARYING, ids bigint[])
RETURNS void AS
$body$
DECLARE 
  select_statement varchar;
  val record;
  link_instance RECORD;
BEGIN
    /* parameters of select_statement:
     * 1- schema_name, 2- table_name, 3- column_name, 4- entity-ids
     * */
	select_statement := $select_links_count$
	SELECT count(*) FROM 
	%1$s.%2$s
	INNER JOIN 
	UNNEST(CAST(%4$L AS bigint[])) entity(id)
	ON entity.id = %3$s
	$select_links_count$;
	
/* Get the links that related to the entity from the source. */
 RAISE NOTICE ' ---------------------------------------- Source ----------------------------------------- ';
 FOR link_instance IN 
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
    INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
    WHERE (tabs.table_name LIKE ('%\_'|| entity_name ||'\_%') OR tabs.table_name LIKE ANY (VALUES ('extr\_'|| entity_name),('docr\_'|| entity_name),('der\_' || entity_name)))
	    AND  tabs.table_type = 'BASE TABLE' 
	    AND cols.column_name LIKE ('%\_src\_%')   
 LOOP
	EXECUTE format(select_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids) INTO val;
	RAISE NOTICE '%.%: % links.', link_instance.table_schema, link_instance.table_name, val; 
 END LOOP;
  
/* Get the links that related to the entity from the destination. */
 RAISE NOTICE ' -------------------------------------- Destination -------------------------------------- ';
 FOR link_instance IN
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
	INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
	WHERE (tabs.table_name LIKE ('%\_'|| entity_name) AND tabs.table_name NOT LIKE ANY(VALUES ('extr\_%'),('docr\_%'),('der\_%'), ('ft\_%')) )
		AND tabs.table_type = 'BASE TABLE'
		AND cols.column_name LIKE '%\_dst\_%' 
 LOOP 	
		EXECUTE format(select_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids) INTO val;
		RAISE NOTICE '%.%: % links.', link_instance.table_schema, link_instance.table_name, val; 
 END LOOP; 
END;
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.get_entity_links_count(character VARYING,bigint[]) IS 
'This function is to count the links that related to certain entity and get the result.
  Parameters: 
 -entity name: name of entity you working on.
 -list of entity-id: ids that you want to count their links.';

---------------------------------------------------------------------------------------------------------
 
/*
######  ######  #######               
#     # #     #    #    #####   ####  
#     # #     #    #    #    # #      
######  #     #    #    #    #  ####  
#     # #     #    #    #####       # 
#     # #     #    #    #      #    # 
######  ######     #    #       ####  
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$

/*--------------------------------function: get_entity_links_count--------------------------------*/
CREATE OR REPLACE FUNCTION public.get_entity_links_count(entity_name character VARYING, ids bigint[])
RETURNS void AS
$body$
DECLARE 
  select_statement varchar;
  val record;
  link_instance RECORD;
BEGIN
    /* parameters of select_statement:
     * 1- schema_name, 2- table_name, 3- column_name, 4- entity-ids
     * */
	select_statement := $select_links_count$
	SELECT count(*) FROM 
	%1$s.%2$s
	INNER JOIN 
	UNNEST(CAST(%4$L AS bigint[])) entity(id)
	ON entity.id = %3$s
	$select_links_count$;
	
/* Get the links that related to the entity from the source. */
 RAISE NOTICE ' ---------------------------------------- Source ----------------------------------------- ';
 FOR link_instance IN 
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
    INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
    WHERE (tabs.table_name LIKE ('%\_'|| entity_name ||'\_%') OR tabs.table_name LIKE ANY (VALUES ('extr\_'|| entity_name),('docr\_'|| entity_name),('der\_' || entity_name)))
	    AND  tabs.table_type = 'BASE TABLE' 
	    AND cols.column_name LIKE ('%\_src\_%')   
 LOOP
	EXECUTE format(select_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids) INTO val;
	RAISE NOTICE '%.%: % links.', link_instance.table_schema, link_instance.table_name, val; 
 END LOOP;
  
/* Get the links that related to the entity from the destination. */
 RAISE NOTICE ' -------------------------------------- Destination -------------------------------------- ';
 FOR link_instance IN
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
	INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
	WHERE (tabs.table_name LIKE ('%\_'|| entity_name) AND tabs.table_name NOT LIKE ANY(VALUES ('extr\_%'),('docr\_%'),('der\_%'), ('ft\_%')) )
		AND tabs.table_type = 'BASE TABLE'
		AND cols.column_name LIKE '%\_dst\_%' 
 LOOP 	
		EXECUTE format(select_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids) INTO val;
		RAISE NOTICE '%.%: % links.', link_instance.table_schema, link_instance.table_name, val; 
 END LOOP; 
END;
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.get_entity_links_count(character VARYING,bigint[]) IS 
'This function is to count the links that related to certain entity and get the result.
  Parameters: 
 -entity name: name of entity you working on.
 -list of entity-id: ids that you want to count their links.';

$bdlink_exec_containt$);
SELECT dblink_disconnect('xmat-tps');
---------------------------------------------------------------------------------------------------------


SELECT public.get_entity_links_count('substances', ARRAY[2744001])




	  
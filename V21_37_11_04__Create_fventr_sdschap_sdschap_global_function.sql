/*
 * The current version of this function accepts sdschap_id as parameter, but it's better to make it accepts a list of sdschap_id as parameter.
 * 1-change type of parameter bigint -> bigint[].
 * 2-change condition: where sdschap_id =  $1 -> where sdschap_id =  ANY($1).
 * 
 * */


CREATE OR REPLACE FUNCTION public.fventr_sdschap_sdschap_subsdschap_global(ventr_src_id bigint[])
 RETURNS TABLE(ventr_id bigint, ventr_src_id bigint, ventr_dst_id bigint, ventr_tp character varying, ventr_co character varying, ventr_nt character varying, ventr_st character varying, ventr_num1 bigint, ventr_num2 bigint, ventr_char1 character varying, ventr_char2 character varying, ventr_float1 double precision, ventr_float2 double precision, ventr_xml text, ventr_dt1 timestamp without time zone, ventr_dt2 timestamp without time zone)
 LANGUAGE sql
AS $function$
WITH RECURSIVE subchap(
    ventr_src_id,
    ventr_dst_id,
    ventr_tp,
    ventr_char1,
    ventr_char2,
    ventr_num1,
    ventr_num2,
    ventr_float1) AS(
  SELECT sdschap.sdschap_id AS ventr_src_id,
         sdschap.sdschap_id AS ventr_dst_id,
         NULL::character varying (700) AS ventr_tp,
         NULL::character varying (4000) AS ventr_char1,
         sdschap.numero::character varying (4000) AS ventr_char2,
         sdschap.sdschap_id AS ventr_num1,
         0::bigint AS ventr_num2,
         CAST(sdschap.view_priority AS double precision) AS ventr_float1
  FROM sdschap
  where sdschap_id =  ANY($1)
  UNION ALL
  SELECT prec.ventr_src_id,
         sdschap.sdschap_id AS ventr_dst_id,
         ess.entr_tp::character varying (700) AS ventr_tp,
         prec.ventr_char2 AS ventr_char1,
         sdschap.numero::character varying (4000) AS ventr_char2,
         prec.ventr_dst_id AS ventr_num1,
         prec.ventr_num2 + 1 AS ventr_num2,
         CAST(sdschap.view_priority AS double precision) AS ventr_float1
  FROM subchap prec
       JOIN entr_sdschap_sdschap ess ON ess.entr_src_id = prec.ventr_dst_id
       JOIN sdschap ON sdschap.sdschap_id = ess.entr_dst_id
       WHERE entr_tp IN ('CHILD_CHAPTER','CHILD_CHAPTER_INHERITED','INSTANCECHAPTERTEMPLATE','CHILD_CHAPTER_INSTANCE'))
      SELECT (mod(subchap.ventr_src_id, 100000000::bigint) * 1000000000 + mod(
        subchap.ventr_dst_id, 1000000000::bigint)) * 100 + 1 AS ventr_id,
             subchap.ventr_src_id,
             subchap.ventr_dst_id,
             subchap.ventr_tp::text || '_TREE' ::text AS ventr_tp,
             NULL::character varying (4000) AS ventr_co,
             NULL::character varying (4000) AS ventr_nt,
             'VALID'::character varying (700) AS ventr_st,
             subchap.ventr_num1,
             subchap.ventr_num2,
             subchap.ventr_char1,
             subchap.ventr_char2,
             subchap.ventr_float1 AS ventr_float1,
             NULL::double precision AS ventr_float2,
             NULL::text AS ventr_xml,
             NULL::TIMESTAMP WITHOUT TIME ZONE AS ventr_dt1,
             NULL::TIMESTAMP WITHOUT TIME ZONE AS ventr_dt2
      FROM subchap
      WHERE subchap.ventr_tp IS NOT NULL
$function$
;
COMMENT ON FUNCTION public.fventr_sdschap_sdschap_subsdschap_global(bigint[])
IS 'Given a list of sdschap_id as parameter, returns all sdschap that are direct or indirect children';


--------------------------------------------------------------------------------------------------------------

/*
######  ######  #######               
#     # #     #    #    #####   ####  
#     # #     #    #    #    # #      
######  #     #    #    #    #  ####  
#     # #     #    #    #####       # 
#     # #     #    #    #      #    # 
######  ######     #    #       ####  
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$

CREATE OR REPLACE FUNCTION public.fventr_sdschap_sdschap_subsdschap(ventr_src_id bigint[])
 RETURNS TABLE(ventr_id bigint, ventr_src_id bigint, ventr_dst_id bigint, ventr_tp character varying, ventr_co character varying, ventr_nt character varying, ventr_st character varying, ventr_num1 bigint, ventr_num2 bigint, ventr_char1 character varying, ventr_char2 character varying, ventr_float1 double precision, ventr_float2 double precision, ventr_xml text, ventr_dt1 timestamp without time zone, ventr_dt2 timestamp without time zone)
 LANGUAGE sql
AS $function$
WITH RECURSIVE subchap(
    ventr_src_id,
    ventr_dst_id,
    ventr_tp,
    ventr_char1,
    ventr_char2,
    ventr_num1,
    ventr_num2,
    ventr_float1) AS(
  SELECT sdschap.sdschap_id AS ventr_src_id,
         sdschap.sdschap_id AS ventr_dst_id,
         NULL::character varying (700) AS ventr_tp,
         NULL::character varying (4000) AS ventr_char1,
         sdschap.numero::character varying (4000) AS ventr_char2,
         sdschap.sdschap_id AS ventr_num1,
         0::bigint AS ventr_num2,
         CAST(sdschap.view_priority AS double precision) AS ventr_float1
  FROM sdschap
  where sdschap_id =  ANY($1)
  UNION ALL
  SELECT prec.ventr_src_id,
         sdschap.sdschap_id AS ventr_dst_id,
         ess.entr_tp::character varying (700) AS ventr_tp,
         prec.ventr_char2 AS ventr_char1,
         sdschap.numero::character varying (4000) AS ventr_char2,
         prec.ventr_dst_id AS ventr_num1,
         prec.ventr_num2 + 1 AS ventr_num2,
         CAST(sdschap.view_priority AS double precision) AS ventr_float1
  FROM subchap prec
       JOIN entr_sdschap_sdschap ess ON ess.entr_src_id = prec.ventr_dst_id
       JOIN sdschap ON sdschap.sdschap_id = ess.entr_dst_id
       WHERE entr_tp IN ('CHILD_CHAPTER','CHILD_CHAPTER_INHERITED','INSTANCECHAPTERTEMPLATE','CHILD_CHAPTER_INSTANCE'))
      SELECT (mod(subchap.ventr_src_id, 100000000::bigint) * 1000000000 + mod(
        subchap.ventr_dst_id, 1000000000::bigint)) * 100 + 1 AS ventr_id,
             subchap.ventr_src_id,
             subchap.ventr_dst_id,
             subchap.ventr_tp::text || '_TREE' ::text AS ventr_tp,
             NULL::character varying (4000) AS ventr_co,
             NULL::character varying (4000) AS ventr_nt,
             'VALID'::character varying (700) AS ventr_st,
             subchap.ventr_num1,
             subchap.ventr_num2,
             subchap.ventr_char1,
             subchap.ventr_char2,
             subchap.ventr_float1 AS ventr_float1,
             NULL::double precision AS ventr_float2,
             NULL::text AS ventr_xml,
             NULL::TIMESTAMP WITHOUT TIME ZONE AS ventr_dt1,
             NULL::TIMESTAMP WITHOUT TIME ZONE AS ventr_dt2
      FROM subchap
      WHERE subchap.ventr_tp IS NOT NULL
$function$
;
COMMENT ON FUNCTION public.fventr_sdschap_sdschap_subsdschap_1(bigint[])
IS 'Given a list of sdschap_id as parameter, returns all sdschap that are direct or indirect children';

$bdlink_exec_containt$);

SELECT dblink_disconnect('xmat-tps');



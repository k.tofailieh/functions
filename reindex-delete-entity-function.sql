

/*
 * This function is to reindex and delete entities.
 * it call reindex_entity_relations to reindex the links and the delete the entities.(in old_ids)
 * extract the enitiy_id that you want to change (duplications for example), and the entity_id you want to replacing by.
 * pass them as parameters with entity name.
 * it is important to keep the order of ids in the lists.
 * 
 * Parameters: 
 *  1- entity_name: table name in the database.(fiche for example).
 *  2- old_ids: the entity_ids that we want to change then delete.
 *  3- new_ids: the entity_ids that we want to put.
 *  */

CREATE OR REPLACE FUNCTION public.reindex_delete_entity(entity_name character VARYING, old_ids bigint[], new_ids bigint[])
RETURNS void AS
$body$
DECLARE 
  delete_statement varchar;
  entity_instance RECORD;
  get_entity_instance_statement varchar;
BEGIN
	/* The statment of deleting entity.
	 * Given parameters: (1) schema_name, (2) table_name, (3) pk column name, (4) list of entity_id that we want to delete. */
  	delete_statement := $delete_entity$
  	DELETE FROM %1$s.%2$s
  	USING (SELECT id FROM UNNEST(CAST(%4$L AS bigint[])) entity(id)) entity
  	WHERE %3$s = entity.id
  	$delete_entity$;
  	
   /* The statment of getting entity_id column name.
    * it searches over information_schema.key_column_usage and get the pk_column.*/
    get_entity_instance_statement := $get_entity_instance$
		SELECT tabs.table_schema, tabs.table_name, cols.column_name, cols.constraint_name 
		FROM information_schema.key_column_usage cols
		INNER JOIN information_schema.TABLES tabs ON (cols.table_catalog = tabs.table_catalog AND cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
		WHERE tabs.table_name LIKE %1$L
	    AND tabs.table_type = 'BASE TABLE' 
	    AND tabs.table_schema IN ('public', 'anx')
	    AND cols.constraint_name LIKE ANY (VALUES ('%%pk%%'), ('\_key%%'), ('key'))
    $get_entity_instance$;
    
    /* call reindex_entity_relations function to reindex the old_ids into new_ids.*/
	PERFORM public.reindex_entity_relations(entity_name, old_ids, new_ids);
    
    /* get entity information (pk column name)*/
	EXECUTE format(get_entity_instance_statement, entity_name) INTO entity_instance;

	/* delete entity.*/
	EXECUTE format(delete_statement, entity_instance.table_schema::varchar, entity_instance.table_name::varchar, entity_instance.column_name::varchar, old_ids );
    RAISE NOTICE 'The old entities of %.%  Deleted!',entity_instance.table_schema, entity_instance.table_name;	
END
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.reindex_delete_entity(entity_name character VARYING, old_ids bigint[], new_ids bigint[]) IS 
'This function is about reindex some entity and then delete it.
 Parameters: 
 1- entity_name: table name in the database.(fiche for example).
 2- old_ids: the entity_ids that we want to change then delete.
 3- new_ids: the entity_ids that we want to put.'


----------------------------------------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------------------------------------

/*
######  ######  #######               
#     # #     #    #    #####   ####  
#     # #     #    #    #    # #      
######  #     #    #    #    #  ####  
#     # #     #    #    #####       # 
#     # #     #    #    #      #    # 
######  ######     #    #       ####  
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$
CREATE OR REPLACE FUNCTION public.reindex_delete_entity(entity_name character VARYING, old_ids bigint[], new_ids bigint[])
RETURNS void AS
$body$
DECLARE 
  delete_statement varchar;
  entity_instance RECORD;
  get_entity_instance_statement varchar;
BEGIN
	/* The statment of deleting entity.
	 * Given parameters: (1) schema_name, (2) table_name, (3) pk column name, (4) list of entity_id that we want to delete. */
  	delete_statement := $delete_entity$
  	DELETE FROM %1$s.%2$s
  	USING (SELECT id FROM UNNEST(CAST(%4$L AS bigint[])) entity(id)) entity
  	WHERE %3$s = entity.id
  	$delete_entity$;
  	
   /* The statment of getting entity_id column name.
    * it searches over information_schema.key_column_usage and get the pk_column.*/
    get_entity_instance_statement := $get_entity_instance$
		SELECT tabs.table_schema, tabs.table_name, cols.column_name, cols.constraint_name 
		FROM information_schema.key_column_usage cols
		INNER JOIN information_schema.TABLES tabs ON (cols.table_catalog = tabs.table_catalog AND cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
		WHERE tabs.table_name LIKE %1$L
	    AND tabs.table_type = 'BASE TABLE' 
	    AND tabs.table_schema IN ('public', 'anx')
	    AND cols.constraint_name LIKE ANY (VALUES ('%%pk%%'), ('\_key%%'), ('key'))
    $get_entity_instance$;
    
    /* call reindex_entity_relations function to reindex the old_ids into new_ids.*/
	PERFORM public.reindex_entity_relations(entity_name, old_ids, new_ids);
    
    /* get entity information (pk column name)*/
	EXECUTE format(get_entity_instance_statement, entity_name) INTO entity_instance;

	/* delete entity.*/
	EXECUTE format(delete_statement, entity_instance.table_schema::varchar, entity_instance.table_name::varchar, entity_instance.column_name::varchar, old_ids );
    RAISE NOTICE 'The old entities of %.%  Deleted!',entity_instance.table_schema, entity_instance.table_name;	
END
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.reindex_delete_entity(entity_name character VARYING, old_ids bigint[], new_ids bigint[]) IS 
'This function is about reindex some entity and then delete it.
 Parameters: 
 1- entity_name: table name in the database.(fiche for example).
 2- old_ids: the entity_ids that we want to change then delete.
 3- new_ids: the entity_ids that we want to put.'

$bdlink_exec_containt$);

SELECT dblink_disconnect('xmat-tps');

----------------------------------------------------------------------------------------------------------------------------

/*---------------------------------function: delete_entity_links--------------------------------*/
CREATE OR REPLACE FUNCTION public.delete_entity_links(entity_name character VARYING, ids bigint[])
RETURNS SETOF record AS
$body$
DECLARE 
  delete_statement varchar;
  val record;
  link_instance RECORD;
  rows_count integer;
BEGIN
	/*
	 * parameters of delete_statement:
     * 1- schema_name, 2- table_name, 3- column_name, 4- entity-ids
     * */
	delete_statement := $delete_links$
    DELETE FROM %1$s.%2$s 
    USING UNNEST(CAST(%4$L AS bigint[])) entity(id) 
    WHERE %1$s.%2$s.%3$s = entity.id;
	$delete_links$;

/* Get relations that links to the entity from the source. */
 RAISE NOTICE ' ---------------------------------------- Source ----------------------------------------- ';
 FOR link_instance IN 
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
    INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
    WHERE (tabs.table_name LIKE ('%\_'|| entity_name ||'\_%') OR tabs.table_name LIKE ANY (VALUES ('extr\_'|| entity_name),('docr\_'|| entity_name),('der\_' || entity_name)))
	    AND  tabs.table_type = 'BASE TABLE' 
	    AND cols.column_name LIKE ('%\_src\_%')   
 LOOP
	EXECUTE format(delete_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids);
	GET DIAGNOSTICS rows_count = ROW_COUNT; 
	RAISE NOTICE '%.%: (%) links deleted.', link_instance.table_schema, link_instance.table_name, rows_count; 
 END LOOP;
  
/* Get relations that links to the entity from the destination. */
 RAISE NOTICE ' -------------------------------------- Destination -------------------------------------- ';
 FOR link_instance IN
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
	INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
	WHERE (tabs.table_name LIKE ('%\_'|| entity_name) AND tabs.table_name NOT LIKE ANY(VALUES ('extr\_%'),('docr\_%'),('der\_%'), ('ft\_%')) )
		AND tabs.table_type = 'BASE TABLE'
		AND cols.column_name LIKE '%\_dst\_%' 
 LOOP 	
		EXECUTE format(delete_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids);
		GET DIAGNOSTICS rows_count = ROW_COUNT; 
		RAISE NOTICE '%.%: (%) links deleted.', link_instance.table_schema, link_instance.table_name, rows_count; 
 END LOOP; 
RETURN;
END
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.delete_entity_links(character VARYING, bigint[]) IS 
'This function is to delete all links that related to certain entity.
 Parameters: 
 -entity name: name of entity you working on.
 -list of entity-id: ids that you want to delete all their links.';

------------------------------------------------------------------------------------------------------------------------------

/*
######  ######  #######               
#     # #     #    #    #####   ####  
#     # #     #    #    #    # #      
######  #     #    #    #    #  ####  
#     # #     #    #    #####       # 
#     # #     #    #    #      #    # 
######  ######     #    #       ####  
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$
CREATE OR REPLACE FUNCTION public.delete_entity_links(entity_name character VARYING, ids bigint[])
RETURNS SETOF record AS
$body$
DECLARE 
  delete_statement varchar;
  val record;
  link_instance RECORD;
  rows_count integer;
BEGIN
	/*
	 * parameters of delete_statement:
     * 1- schema_name, 2- table_name, 3- column_name, 4- entity-ids
     * */
	delete_statement := $delete_links$
    DELETE FROM %1$s.%2$s 
    USING UNNEST(CAST(%4$L AS bigint[])) entity(id) 
    WHERE %1$s.%2$s.%3$s = entity.id;
	$delete_links$;

	
/* Get relations that links to the entity from the source. */
 RAISE NOTICE ' ---------------------------------------- Source ----------------------------------------- ';
 FOR link_instance IN 
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
    INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
    WHERE (tabs.table_name LIKE ('%\_'|| entity_name ||'\_%') OR tabs.table_name LIKE ANY (VALUES ('extr\_'|| entity_name),('docr\_'|| entity_name),('der\_' || entity_name)))
	    AND  tabs.table_type = 'BASE TABLE' 
	    AND cols.column_name LIKE ('%\_src\_%')   
 LOOP
	EXECUTE format(delete_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids);
	GET DIAGNOSTICS rows_count = ROW_COUNT; 
	RAISE NOTICE '%.%: (%) links deleted.', link_instance.table_schema, link_instance.table_name, rows_count; 
 END LOOP;
  
/* Get relations that links to the entity from the destination. */
 RAISE NOTICE ' -------------------------------------- Destination -------------------------------------- ';
 FOR link_instance IN
 	SELECT tabs.table_schema, tabs.table_name, cols.column_name 
 	FROM information_schema.COLUMNS cols 
	INNER JOIN information_schema.TABLES tabs ON (cols.table_schema = tabs.table_schema AND cols.table_name = tabs.table_name)
	WHERE (tabs.table_name LIKE ('%\_'|| entity_name) AND tabs.table_name NOT LIKE ANY(VALUES ('extr\_%'),('docr\_%'),('der\_%'), ('ft\_%')) )
		AND tabs.table_type = 'BASE TABLE'
		AND cols.column_name LIKE '%\_dst\_%' 
 LOOP 	
		EXECUTE format(delete_statement, link_instance.table_schema, link_instance.table_name, link_instance.column_name, ids);
		GET DIAGNOSTICS rows_count = ROW_COUNT; 
		RAISE NOTICE '%.%: (%) links deleted.', link_instance.table_schema, link_instance.table_name, rows_count; 
 END LOOP; 
RETURN;
END
$body$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.delete_entity_links(character VARYING, bigint[]) IS 
'This function is to delete all links that related to certain entity.
 Parameters: 
 -entity name: name of entity you working on.
 -list of entity-id: ids that you want to delete all their links.';

$bdlink_exec_containt$);
SELECT dblink_disconnect('xmat-tps');
------------------------------------------------------------------------------------------------------------------------------


SELECT delete_entity_links('substances', ARRAY[2744001]);
CREATE OR REPLACE FUNCTION public.create_index_test(col_name varchar, index_type varchar, tab_name varchar DEFAULT '')
    RETURNS void
    LANGUAGE plpgsql
AS
$function$
DECLARE
    record_instance     RECORD;
    get_index_statement varchar;
BEGIN

    get_index_statement := $get_indexes$
   SELECT 
   $get_indexes$;
    FOR record_instance IN SELECT TABLES.table_schema, TABLES.table_name, COLUMNS.column_name
                           FROM information_schema.COLUMNS /* Columns tpsuid of tables that are temporal (or ft tables)*/
                                    INNER JOIN INFORMATION_SCHEMA.TABLES ON (TABLES.table_schema, TABLES.table_name) =
                                                                            (COLUMNS.table_schema, COLUMNS.table_name)
                               AND TABLES.table_type = 'BASE TABLE'
                               AND COLUMNS.column_name = col_name
                               AND TABLES.table_name LIKE COALESCE(NULLIF(tab_name, ''), '%')
                               AND TABLES.table_schema IN ('public', 'anx')

        LOOP

            RAISE NOTICE '%, %, %', record_instance.table_schema, record_instance.table_name ,record_instance.column_name;

        END LOOP;
END
$function$;

COMMENT ON FUNCTION public.create_index_test() IS
    'This function about data cleaning.
     It checks through these kinds of tables(entr_, extr_, docr_, der_) on columns that ends with (_tp) for null values.
     Adds NOT NULL constraint if the table has no NULL values, otherwise it will raises an error message.';

SELECT create_index_test('ns', '');


CREATE OR REPLACE FUNCTION public.add_notnull_constraint_r_tp()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
		table_schema VARCHAR;
		table_name VARCHAR;
		column_name VARCHAR;
		record_instance RECORD;
		constraint_statement VARCHAR;
		exception_message VARCHAR;
		myrec RECORD ;
BEGIN
    FOR record_instance IN SELECT
								columns.table_schema,
								columns.table_name,
								columns.column_name,
								columns.is_nullable
							FROM
								information_schema.columns
							INNER JOIN information_schema.tables
							  ON
								(tables.table_schema,
								tables.table_name) =
								(columns.table_schema,
								columns.table_name)
								
							AND tables.table_type = 'BASE TABLE'
							WHERE
								columns.table_name ILIKE ANY (VALUES ('entr_%'),('extr_%'),('docr_%'),('der_%')) -- filtering on these kinds of tables
								 AND columns.column_name ILIKE ('%_tp')                     						 -- filtering on this kind of columns
								 AND columns.is_nullable = 'YES' 													 -- get the columns that missed not-null constrain
        LOOP
            table_schema := record_instance.table_schema;
            table_name := record_instance.table_name;
            column_name := record_instance.column_name;

            constraint_statement := $$ALTER TABLE $$ || table_schema || $$."$$ || table_name || $$" ALTER COLUMN $$ ||
                                    column_name || $$ SET NOT NULL;$$;
                                   
           EXECUTE $$select 1 FROM $$ || table_schema || $$."$$ || table_name || $$" WHERE $$ || column_name || $$ ISNULL; $$
           INTO myrec;

		   IF myrec IS NOT NULL THEN
		   		RAISE NOTICE '%: Field, this table has null values.', table_name;
		   ELSE 
	            BEGIN
		        EXECUTE constraint_statement;
                RAISE NOTICE '%: Done.', table_name ;
                
		    	EXCEPTION WHEN OTHERS THEN
			        GET STACKED DIAGNOSTICS exception_message := MESSAGE_TEXT;
			        RAISE EXCEPTION '%', exception_message; 
			       END;
	      END IF;
	     END LOOP;
END
$function$;

COMMENT ON FUNCTION public.add_notnull_constraint_r_tp() IS 
'This function about data cleaning.
 it checks through these kinds of tables(entr_, extr_, docr_, der_) on columns that ends with (_tp) for null values.
 and adds NOT NULL constraint if the table has no NULL values, otherwise it rasies an error message.';


----------------------------------------------------------------------------------------------
-- DPTps
CREATE OR REPLACE FUNCTION tps.add_notnull_constraint_r_tp()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
		table_schema VARCHAR;
		table_name VARCHAR;
		column_name VARCHAR;
		record_instance RECORD;
		constraint_statement VARCHAR;
		exception_message VARCHAR;
		myrec RECORD ;
BEGIN
    FOR record_instance IN SELECT
								columns.table_schema,
								columns.table_name,
								columns.column_name,
								columns.is_nullable
							FROM
								information_schema.columns
							INNER JOIN information_schema.tables
							  ON
								(tables.table_schema,
								tables.table_name) =
								(columns.table_schema,
								columns.table_name)
								
							AND tables.table_type = 'BASE TABLE'
							WHERE
								columns.table_name ILIKE ANY (VALUES ('entr_%'),('extr_%'),('docr_%'),('der_%')) -- filtering on these kinds of tables
								 AND columns.column_name ILIKE ('%_tp')                     						 -- filtering on this kind of columns
								 AND columns.is_nullable = 'YES' 													 -- get the columns that missed not-null constrain
        LOOP
            table_schema := record_instance.table_schema;
            table_name := record_instance.table_name;
            column_name := record_instance.column_name;

            constraint_statement := $$ALTER TABLE $$ || table_schema || $$."$$ || table_name || $$" ALTER COLUMN $$ ||
                                    column_name || $$ SET NOT NULL;$$;
                                   
           EXECUTE $$select 1 FROM $$ || table_schema || $$."$$ || table_name || $$" WHERE $$ || column_name || $$ ISNULL; $$
           INTO myrec;

		   IF myrec IS NOT NULL THEN
		   		RAISE NOTICE '%: Field, this table has null values.', table_name;
		   ELSE 
	            BEGIN
		        EXECUTE constraint_statement;
                RAISE NOTICE '%: Done.', table_name ;
		    	EXCEPTION WHEN OTHERS THEN
			        GET STACKED DIAGNOSTICS exception_message := MESSAGE_TEXT;
			        RAISE EXCEPTION '%', exception_message; 
			       END;
	      END IF;
	     END LOOP;
END
$function$;

COMMENT ON FUNCTION tps.add_notnull_constraint_r_tp() IS 
'This function about data cleaning.
 it checks through these kinds of tables(entr_, extr_, docr_, der_) on columns that ends with (_tp) for null values.
 and adds NOT NULL constraint if the table has no NULL values, otherwise it rasies an error message.';






/*
 * This Function is for inserting a new translations for INCIs
 * and creating the link between them. 
 * 
 * Before running this function you have to prepare the new Translations:
 * - Put them in import_line table and give them a reference in 'imp_ref'. 
 * - Put the matching key (INCI name) in 'value_txt2' and the translation in 'value_txt1'.
 * - Check if they are exist in Database.
 * - For the ones that exist in Database, put 'imp_warn' = True, or you can pass their Ids (inci_id or imp_id) via excepted_incis, excepted_incis parameters.
 * 
 */
/* CREATE */
CREATE OR REPLACE FUNCTION public.insert_translations_for_inci(country character varying, translations_ref character varying DEFAULT ''::character varying, excepted_translatons integer[] DEFAULT ARRAY[0], excepted_incis integer[] DEFAULT ARRAY[0])
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE

	inserted_id integer;
	last_tr_id integer;
	last_relation_id integer;
	get_last_tr_id_statement CHARACTER VARYING;
	translation_insert_statement CHARACTER VARYING;
	relation_insert_statement CHARACTER VARYING;
	translation_record RECORD;
	
 
BEGIN
	/*excepted_incis and excepted_translatons is an optional parameters, you can left them or pass a filled arrays to them. */
	IF CARDINALITY(excepted_incis) =0 OR CARDINALITY(excepted_translatons) =0 THEN 
	RAISE EXCEPTION 'should not pass an empty arrays for excepted incis and excepted translations';
	END IF;
	
	/* get last inserted (entity and relation) Ids */
	get_last_tr_id_statement := $$SELECT  max(it.incitranslation_id) AS last_id FROM anx.incitranslation it;$$;
	EXECUTE get_last_tr_id_statement INTO last_tr_id;
	
	get_last_tr_id_statement := $$SELECT  max(eii.entr_id) AS last_id FROM anx.entr_inci_incitranslation eii;$$;
	EXECUTE get_last_tr_id_statement INTO last_relation_id;


	FOR translation_record IN SELECT il.value_txt1 AS new_translation,i.inciname, i.inci_id
							  FROM "import".import_line il
							  INNER JOIN anx.inci i ON trim(i.inciname) ILIKE trim(il.value_txt2) AND i.inci_id != ANY(excepted_incis)
							  WHERE imp_ref = translations_ref AND (il.imp_warn ISNULL OR  il.imp_warn = FALSE)  AND il.imp_id != ANY(excepted_translatons) 
	LOOP 
		last_tr_id := last_tr_id +1;
		last_relation_id := last_relation_id +1;
	
		/* inserting translation and creating a link for it. */
		translation_insert_statement := $$insert into anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, country) values ($$ || last_tr_id::int || $$,'$$ || translation_record.new_translation  || $$','$$ || upper(translation_record.new_translation ) || $$','$$|| country || $$') RETURNING incitranslation_id;$$;
		EXECUTE translation_insert_statement INTO inserted_id;
		
	
		relation_insert_statement := $$insert into anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp) values ($$|| last_relation_id ||$$, $$ || translation_record.inci_id::integer || $$,$$ || inserted_id::integer || $$,'TRANSLATION');$$;
		EXECUTE relation_insert_statement;
		RAISE NOTICE '>%: {%} Inserted and Linked to {%} as {%} Translation.', last_tr_id, translation_record.new_translation, translation_record.inciname, country;
		
	END LOOP;
END
$function$
;
COMMENT ON FUNCTION public.insert_translations_for_inci(country character varying, translations_ref character varying, excepted_translatons integer[], excepted_incis integer[]) IS
'This function is to create and link a new translations with INCIs depends on INCI Name.';


/* RUN */
--------------------------------------------------------------------------------------------------------------------------------------------------
/* 'test 929' referene has Trivial Data.*/
SELECT insert_translations_for_inci('CA','test 929');

/* 'test 929-2' referene has Botanical Data.*/
SELECT insert_translations_for_inci('CA','test 929-2');
SELECT insert_translations_for_inci('US','test 929-2')




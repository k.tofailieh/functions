create function add_index() returns void
    language plpgsql
as
$fun$
DECLARE
    record_instance        RECORD;
    table_name             VARCHAR;
    column_name            VARCHAR;
    schema_name            VARCHAR;
    create_index_statement VARCHAR;

BEGIN

    FOR record_instance IN
        SELECT columns.table_schema,
               columns.table_name,
               columns.column_name,
               pg_indexes.indexname

        FROM information_schema.columns
                 LEFT JOIN pg_indexes
                           ON (pg_indexes.schemaname, pg_indexes.tablename) =
                              (columns.table_schema, columns.table_name)
                               AND pg_indexes.indexname ILIKE ('%_tpstill')
        WHERE columns.column_name ILIKE ('%tpstill')
          AND pg_indexes.indexname ISNULL

        LOOP
            table_name := record_instance.table_name;
            column_name := record_instance.column_name;
            schema_name := record_instance.table_schema;
            create_index_statement := $$CREATE INDEX $$ || $$i_$$ || table_name || $$_$$ || column_name || $$ ON $$ ||
                                      schema_name || $$."$$ ||
                                      table_name || $$" USING btree ($$ || column_name || $$);$$;
            BEGIN
            RAISE NOTICE '%', create_index_statement;
--             EXECUTE create_index_statement;
            EXCEPTION WHEN OTHERS THEN
                RAISE EXCEPTION '%', create_index_statement;
            END;
        END LOOP;
END
$fun$;

alter function add_index() owner to horsprod;

